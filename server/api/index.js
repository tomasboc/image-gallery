const express = require('express');

const images = require('./images');

const api = express.Router();

api.use(`/images`, images);

module.exports = api;
