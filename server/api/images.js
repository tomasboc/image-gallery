const express = require('express');

const images = express.Router();

const Images = require('../controllers/Images');

images.get('/load', (req, res) => {
    Images.getImages()
        .then(images => res.status(200).json(images))
        .catch(res.error);
});

images.get('/load_more/:page', (req, res) => {
    const { page } = req.params;
    Images.getMoreImages(page)
        .then(images => res.status(200).json(images))
        .catch(res.error);
});

module.exports = images;
