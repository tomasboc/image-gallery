const rp = require('request-promise');

const { FLICKR_API_URL, FLICKR_API_KEY } = require('../constants');

module.exports = {
    getImages() {
        const options = {
            uri: `${FLICKR_API_URL}?method=flickr.photos.getRecent&api_key=${FLICKR_API_KEY}&format=json&extras='description, license, date_upload, date_taken, owner_name, icon_server, original_format, last_update, geo, tags, machine_tags, o_dims, views, media, path_alias, url_sq, url_t, url_s, url_q, url_m, url_n, url_z, url_c, url_l, url_o'&nojsoncallback=1`,
            method: 'GET',
            json: true,
            contentType: 'application/json',
            dataType: 'json'
        };

        return rp(options);
    },

    getMoreImages(page) {
        const options = {
            uri: `${FLICKR_API_URL}?method=flickr.photos.getRecent&api_key=${FLICKR_API_KEY}&page=${page}&format=json&extras='description, license, date_upload, date_taken, owner_name, icon_server, original_format, last_update, geo, tags, machine_tags, o_dims, views, media, path_alias, url_sq, url_t, url_s, url_q, url_m, url_n, url_z, url_c, url_l, url_o'&nojsoncallback=1`,
            method: 'GET',
            json: true,
            contentType: 'application/json',
            dataType: 'json'
        };

        return rp(options);
    },
};
