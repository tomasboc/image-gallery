export const IMAGES_LOADED = 'imagesLoaded';
export const MORE_IMAGES_LOADED = 'moreImagesLoaded';

export const SHOW_ZEN_MODE = 'showZenMode';
export const HIDE_ZEN_MODE = 'hideZenMode';
