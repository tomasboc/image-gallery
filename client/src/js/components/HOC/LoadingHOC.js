import React from 'react';

import LoadingIcon from '../common/LoadingIcon';

import '../../../style/components/HOC/LoadingHOC.less';

const isEmpty = prop => (
  prop === null
    || prop === undefined
    || (Object.prototype.hasOwnProperty.call(prop, 'length') && prop.length === 0)
    || (prop.constructor === Object && Object.keys(prop).length === 0)
);

const LoadingHOC = loadingProp => WrappedComponent => (props) => {
  const { [loadingProp]: loadingProperty } = props;

  return isEmpty(loadingProperty) ? <LoadingIcon /> : <WrappedComponent {...props} />;
};


export default LoadingHOC;
