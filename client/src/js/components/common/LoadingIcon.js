import React from 'react';
import { string } from 'prop-types';

import '../../../style/components/common/LoadingIcon.less';

const LoadingIcon = (props) => {
  const { size } = props;
  const classnames = size ? `LoadingIcon ${size}` : 'LoadingIcon';

  return (
    <div className={classnames} />
  );
};

LoadingIcon.propTypes = {
  size: string,
};

LoadingIcon.defaultProps = {
  size: '',
};

export default LoadingIcon;
