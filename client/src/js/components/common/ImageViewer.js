import React, { Component } from 'react';
import {
  string, func, bool,
} from 'prop-types';

import LoadingIcon from './LoadingIcon';

import '../../../style/components/common/ImageViewer.less';

export default class ImageViewer extends Component {
    static propTypes = {
      source: string,
      source2: string,
      height: string,
      ownername: string,
      title: string,
      onClick: func,
      description: bool,
      className: string,
    };

  static defaultProps = {
    source: '',
    source2: '',
    height: '',
    ownername: '',
    title: '',
    onClick: () => {},
    description: false,
    className: '',
  };

    state = {
      isImageLoaded: false,
    };

    onLoadImage() {
      const isImageLoaded = true;
      this.setState({ isImageLoaded });
    }

    onClickImage() {
      const { onClick } = this.props;
      if (onClick) onClick();
    }

    render() {
      const {
        source, source2, height, ownername, title, description, className,
      } = this.props;
      const { isImageLoaded } = this.state;

      const style = {
        minHeight: `${height}px`,
        maxHeight: `${height}px`,
      };

      let descriptionEl = (
        <div className="image-description">
          <title className="image-title">{title}</title>
          <span className="image-owner">{ownername}</span>
        </div>
      );

      if (!description) {
        descriptionEl = null;
      }

      let src = source;

      if (source2) {
        src = `${source2} 375w, ${source} 1500w`;
      }

      const classnames = className ? `inner-container ${className}` : 'inner-container';

      if (isImageLoaded) {
        return (
          <button type="button" className="ImageViewer" onClick={this.onClickImage.bind(this)}>
            <section className={classnames}>
              {descriptionEl}
              <img alt="loading" className="image" src={src} srcSet={src} />
            </section>
          </button>
        );
      }

      return (
        <button type="button" className="ImageViewer" onClick={this.onClickImage.bind(this)}>
          <section className={classnames} style={style}>
            <LoadingIcon size="small" />
            <img
              src={source}
              alt="loading"
              className="hide-image"
              onLoad={this.onLoadImage.bind(this)}
            />
          </section>
        </button>
      );
    }
}
