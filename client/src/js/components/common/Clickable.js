import React, { Component } from 'react';
import {
  func, bool, string, element,
} from 'prop-types';

import '../../../style/components/common/Clickable.less';

export default class Clickable extends Component {
    static propTypes = {
      onClick: func.isRequired,
      disabled: bool,
      title: string,
      className: string,
      children: element.isRequired,
    };

  static defaultProps = {
    disabled: false,
    title: '',
    className: '',
  };

  renderTitle() {
    const { title } = this.props;

    if (title) {
      return (
        <span className="tooltip">{title}</span>
      );
    }
    return null;
  }

  render() {
    const {
      onClick, children, disabled, className,
    } = this.props;

    const classnames = className ? `Clickable ${className}` : 'Clickable';

    return (
      <button
        className={classnames}
        disabled={disabled}
        onClick={onClick}
        type="button"
      >
        {this.renderTitle()}
        {children}
      </button>
    );
  }
}
