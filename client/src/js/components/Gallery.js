import React, { Component } from 'react';
import { connect } from 'react-redux';
import { shape, func } from 'prop-types';

import ImageViewer from './common/ImageViewer';
import LoadingHOC from './HOC/LoadingHOC';

import { loadMoreImages } from '../actions/images';
import { showZenMode } from '../actions/zenMode';

import '../../style/components/Gallery.less';

@connect()
@LoadingHOC('images')
export default class Gallery extends Component {
    static propTypes = {
      images: shape,
      dispatch: func,
    };

    static defaultProps = {
      images: {},
      dispatch: () => {},
    };

    constructor(props) {
      super(props);
      this.openZenMode = this.openZenMode.bind(this);
      this.loadMore = this.loadMore.bind(this);
    }

    loadMore() {
      const { images, dispatch } = this.props;
      const { page, pages } = images;

      if (page < pages) {
        dispatch(loadMoreImages(page + 1));
      }
    }

    openZenMode(item) {
      const { dispatch } = this.props;

      dispatch(showZenMode(item));
    }

    renderImages() {
      const { images } = this.props;
      const { photo } = images;
      const collection = photo.map(((item, index) => {
        const {
          id, url_s: urlS, height_s: heightS, ownername, title,
        } = item;
        const randomId = (id + Math.floor(Math.random() * 100 + index)).toString();
        return (
          <div key={randomId}>
            <ImageViewer
              source={urlS}
              height={heightS}
              ownername={ownername}
              title={title}
              onClick={() => this.openZenMode(item)}
              description
            />
          </div>
        );
      }));

      return (
        <section className="gallery-card">
          {collection}
        </section>
      );
    }

    renderLoadMore() {
      const { images } = this.props;
      const { page, pages } = images;

      if (page < pages) {
        return (
          <button type="button" onClick={this.loadMore} className="load-more">Load More</button>
        );
      }
      return null;
    }

    render() {
      return (
        <article className="Gallery">
          {this.renderImages()}
          {this.renderLoadMore()}
        </article>
      );
    }
}
