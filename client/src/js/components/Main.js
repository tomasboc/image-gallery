import React, { Component } from 'react';
import { connect } from 'react-redux';
import { func, shape } from 'prop-types';

import Gallery from './Gallery';
import Clickable from './common/Clickable';
import ImageViewer from './common/ImageViewer';

import { loadImages } from '../actions/images';
import { hideZenMode } from '../actions/zenMode';

import '../../style/components/Main.less';

@connect(({ images, zenMode, dispatch }) => ({ images, zenMode, dispatch }))
export default class Main extends Component {
  static propTypes = {
    dispatch: func,
    images: shape,
    zenMode: shape,
  };

  static defaultProps = {
    dispatch: () => {},
    images: {},
    zenMode: {},
  };

  constructor(props) {
    super(props);
    this.onClickCloseZenLayer = this.onClickCloseZenLayer.bind(this);
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(loadImages());
  }

  onClickCloseZenLayer() {
    const { dispatch } = this.props;

    dispatch(hideZenMode());
  }

  renderGallery() {
    const { images } = this.props;

    return (
      <Gallery images={images} />
    );
  }

  renderZenContainer() {
    const { zenMode } = this.props;

    if (zenMode && zenMode.zenImage) {
      const {
        url_s: urlS, url_m: urlM, height_m: heightM, ownername, title,
      } = zenMode.zenImage;

      return (
        <section className="zen-layer">
          <ImageViewer
            source={urlM}
            source2={urlS}
            height={heightM}
            ownername={ownername}
            title={title}
            description={false}
            className="lightbox-image"
          />
          {this.renderZenClose()}
        </section>
      );
    }
    return null;
  }

  renderZenClose() {
    return (
      <Clickable
        className="close-button"
        onClick={this.onClickCloseZenLayer}
        title="Close"
      >
                X
      </Clickable>
    );
  }

  render() {
    return (
      <article className="Main">
        {this.renderGallery()}
        {this.renderZenContainer()}
      </article>
    );
  }
}
