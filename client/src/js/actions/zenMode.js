import { SHOW_ZEN_MODE, HIDE_ZEN_MODE } from '../constants';

export const showZenMode = zenImage => ({ type: SHOW_ZEN_MODE, zenImage });

export const hideZenMode = () => ({ type: HIDE_ZEN_MODE });
