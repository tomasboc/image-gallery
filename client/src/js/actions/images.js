import fetch from 'cross-fetch';

import { IMAGES_LOADED, MORE_IMAGES_LOADED } from '../constants';

export const loadImages = () => dispatch => fetch('/api/images/load')
  .then(results => results.json())
  .then((results) => {
    const images = results.photos;
    dispatch({ type: IMAGES_LOADED, images });
  })
  .catch((err) => {
    console.error(err);
  });

export const loadMoreImages = page => dispatch => fetch(`/api/images/load_more/${page}`)
  .then(results => results.json())
  .then((results) => {
    const images = results.photos;
    dispatch({ type: MORE_IMAGES_LOADED, images });
  })
  .catch((err) => {
    console.error(err);
  });
