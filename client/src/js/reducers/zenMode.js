import { SHOW_ZEN_MODE, HIDE_ZEN_MODE } from '../constants';

export default (state = [], { type, zenImage }) => {
  switch (type) {
  case SHOW_ZEN_MODE:
    return Object.assign({}, state, { zenImage });
  case HIDE_ZEN_MODE:
    return Object.assign({}, state, { zenImage: null });
  default:
    return state;
  }
};
