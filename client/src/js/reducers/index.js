import images from './images';
import zenMode from './zenMode';

export default {
  images,
  zenMode,
};
