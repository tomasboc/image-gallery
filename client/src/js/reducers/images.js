import { IMAGES_LOADED, MORE_IMAGES_LOADED } from '../constants';

export default (state = {}, action) => {
  const { type, images } = action;

  switch (type) {
  case IMAGES_LOADED:
    return Object.assign({}, state, images);
  case MORE_IMAGES_LOADED:
    return Object.assign({}, state, images, { photo: state.photo.concat(images.photo) });
  default:
    return state;
  }
};
