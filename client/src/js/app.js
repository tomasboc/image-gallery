import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import { Router, Route } from 'react-router';
import { createBrowserHistory } from 'history';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';

import reducers from './reducers';

import Main from './components/Main';

const baseMiddlewares = [thunk];
if (process.env.log) {
  const logger = createLogger();
  baseMiddlewares.push(logger);
}
const middlewares = applyMiddleware(...baseMiddlewares);
const store = createStore(
  combineReducers({
    ...reducers,
    routing: routerReducer,
  }),
  middlewares,
);

const history = syncHistoryWithStore(createBrowserHistory(), store);

render(
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={Main} />
    </Router>
  </Provider>,
  document.getElementById('gallery'),
);
